/*
 *  This software is free.
 */

package fastfourier;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JFrame;

/*

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author amacrae
 */
public class FastFourier extends JFrame
{

    FourierDrawPanel nPanel = new FourierDrawPanel();
    public static void main(String[] args)
    {
        FastFourier mf = new FastFourier();
        mf.setVisible(true);
        mf.init();        
    }

    public FastFourier()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(850, 700);
        setTitle(" Fourier Tester ");
        Container content = getContentPane();

        content.add(nPanel, BorderLayout.CENTER);

    }
    public void init()
    {
        nPanel.setFocusable(true);
    }
}

