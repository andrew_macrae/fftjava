/*
 *  This software is free.
 */

package fastfourier;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class FourierDrawPanel extends JPanel implements Runnable
{
    // define globals here
    
    double operations = 0;
    int recursionCount = 0;
    
    Thread fred;
    private int lX,W,tYscope,hScope,tYspect,hSpect;
    private int nLines;
    private int tsLen = 12; // length of time series = 2^tsLen
    private boolean doFFT = true;
    
    private double[] timeSeries;
    private Complex[] spectrum;
    
    public FourierDrawPanel()
    {
        init();
    }

    public void init()
    {
        lX = 20;
        W = 800;
        hScope = hSpect = 300;
        tYscope = 20;
        tYspect = 30+tYscope+hScope;
        nLines = 6;
        timeSeries = getTS(tsLen);
        //spectrum = new double[timeSeries.length/2];
        
        fred = new Thread(this);
        fred.start();
    }

    public void run()
    {
        while (true)
        {
            // do stuff
            try
            {
                Thread.sleep(20);
                repaint();
            } catch (InterruptedException e)
            {
                System.out.println(e.toString());
            }
        }
    }

    @Override
    public void paintComponent(Graphics g)
    {
        g.setColor(Color.black);
        g.fillRect(lX-10, tYscope-10, W+20, hScope+20);
        g.fillRect(lX-10, tYspect-10, W+20, hSpect+20);
        g.setColor(Color.white);
        g.drawRect(lX, tYscope, W, hScope);
        for(int i = 1;i<nLines;i++)
        {
            int dH = hScope/nLines;
            int dW = W/nLines;
            g.drawLine(lX,tYscope+i*dH,lX+W,tYscope+i*dH);
            g.drawLine(lX+i*dW,tYscope,lX+i*dW ,tYscope+hScope);
        }
        g.setColor(Color.yellow);
        g.drawRect(lX, tYspect, W, hSpect);
        for(int i = 1;i<nLines;i++)
        {
            int dH = hSpect/nLines;
            int dW = W/nLines;
            g.drawLine(lX,tYspect+i*dH,lX+W,tYspect+i*dH);
            g.drawLine(lX+i*dW,tYspect,lX+i*dW ,tYspect+hSpect);
        }        
        
        timeSeries = getTS(tsLen);
        g.setColor(Color.red);
        plotTrace(g,timeSeries,tYscope);
   
        if(doFFT)
        {
            long t0 = System.currentTimeMillis();
            spectrum = fft(timeSeries);
            System.out.println("Computed FFT in "+(System.currentTimeMillis()-t0)+"ms.");
            g.setColor(Color.cyan);
            plotTrace(g,spectrum,tYspect,(int)Math.pow(2,9));            
        }
        else
        {
            long t0 = System.currentTimeMillis();
            spectrum = dft(timeSeries);
            System.out.println("Computed DFT in "+(System.currentTimeMillis()-t0)+"ms.");
            g.setColor(Color.cyan);
            plotTrace(g,spectrum,tYspect);           
        }
    }
    
    public void plotTrace(Graphics g,double[] data,int topY)
    {
        double n = (double)data.length;
        double max = -10000,min=-max;
        for(int i = 0;i<n;i++)
        {
            if(data[i]>max)max=data[i];
            if(data[i]<min)min=data[i];
        }
        double hScale = W/n;        
        double vScale = hScope/Math.max(1,max-min);
        int dummy = -1;
        for(int i = 0;i<n-1;i++)
        {
            g.drawLine((int)(lX+i*hScale),topY+hScope-(int)(vScale*(data[i]-min)),(int)(lX+(i+1)*hScale),topY+hScope-(int)(vScale*(data[i+1]-min)));
        }       
    }
    public void plotTrace(Graphics g,Complex[] dat,int topY)
    {
        double n = (double)dat.length;
        double data[] = new double[(int)n];
        for(int i = 0;i<n;i++)
        {
            data[i] = dat[i].abs();
        }
        double max = -10000,min=-max;
        for(int i = 0;i<n;i++)
        {
            if(data[i]>max)max=data[i];
            if(data[i]<min)min=data[i];
        }
        double hScale = W/n;        
        double vScale = hScope/Math.max(1,max-min);
        int dummy = -1;
        for(int i = 0;i<n-1;i++)
        {
            g.drawLine((int)(lX+i*hScale),topY+hScope-(int)(vScale*(data[i]-min)),(int)(lX+(i+1)*hScale),topY+hScope-(int)(vScale*(data[i+1]-min)));
        }       
    }
    public void plotTrace(Graphics g,Complex[] dat,int topY,int mx)
    {
        double n = (int)Math.min(mx,dat.length);
        double data[] = new double[(int)n];
        for(int i = 0;i<n;i++)
        {
            data[i] = dat[i].abs();
        }
        double max = -10000,min=-max;
        for(int i = 0;i<n;i++)
        {
            if(data[i]>max)max=data[i];
            if(data[i]<min)min=data[i];
        }
        double hScale = W/n;        
        double vScale = hScope/Math.max(1,max-min);
        int dummy = -1;
        for(int i = 0;i<n-1;i++)
        {
            g.drawLine((int)(lX+i*hScale),topY+hScope-(int)(vScale*(data[i]-min)),(int)(lX+(i+1)*hScale),topY+hScope-(int)(vScale*(data[i+1]-min)));
        }       
    }    
    public Complex[] fft(double[] ts)
    {
        int N = ts.length;
        
        
        if(N==1)
        {
            return new Complex[] {toComplex(ts[0])};              
        }
        else
        {  
            if (N % 2 != 0) { throw new RuntimeException("N is not a power of 2"); }
            double[] even = new double[N/2];
            for (int i = 0; i < N/2; i++) 
            {
                even[i] = ts[2*i];
            }            
            Complex[] q = fft(even);
            double[] odd = even;
            for (int i = 0; i < N/2; i++) 
            {
                odd[i] = ts[2*i+1];
            }            
            Complex[] p = fft(odd);
            Complex[] y = new Complex[N];
            
            for (int k = 0; k < N/2; k++) 
            {
                double kth = -2 * k * Math.PI / N;
                Complex wk = new Complex(Math.cos(kth), Math.sin(kth));
                y[k] = q[k].plus(wk.times(p[k]));
                y[k + N/2] = q[k].minus(wk.times(p[k])); // Here she is ...
            }
            return y;            
                  
        }
    }
        
    public Complex toComplex(double d)
    {
        return new Complex(d,0);
    }
    public double[] evens(double[] arr)
    {
        double[] ret = new double[arr.length/2];
        for(int i = 0;i<arr.length;i+=2)
        {
            ret[i/2] = arr[i];
        }
        return ret;
    }
    public double[] odds(double[] arr)
    {
        double[] ret = new double[arr.length/2];
        for(int i = 0;i<ret.length;i++)
        {
            ret[i] = arr[2*i+1];
        }
        return ret;
    }
    
    public Complex[] dft(double[] ts)
    {
        Complex[] fs = new Complex[ts.length];
        for(int k = 0;k<ts.length;k++)
        {
            fs[k] = new Complex(0,0);
            for(int i = 0;i<ts.length;i++)
            {
                fs[k] = fs[k].plus(new Complex(0,-2*Math.PI*k*i/ts.length).exp().times(ts[i]));
            }
        }
        
        return fs;
    }
    
    public double[] getTS(int pow2)
    {
        double[] ret = new double[(int)Math.pow(2,pow2)];
        for(int i = 0;i<ret.length;i++)
        {
            ret[i] = 1.0*Math.sin(2*Math.PI*8.5*i/ret.length)+1.25*Math.sin(2*Math.PI*25*i/ret.length)+0.85*Math.sin(2*Math.PI*95*i/ret.length)+Math.random()*1.4;
        }
        return ret;
    }

}